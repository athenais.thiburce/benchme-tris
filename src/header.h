#ifndef MON_FICHIER_ /* L'écriture ici présente n'est pas universelle */
#define MON_FICHIER_ /* Edit suite aux corrections des posts suivants -> */
/*Il est conseillé d'utiliser MON_FICHIER : pas de underscore au début */

/* Tu places ici les prototypes de tes fonctions */

/**
 * \fn tri_insertion(float arr[], int n)
 * \brief {Déclaration desz méthodes qui vont être appelé dans d'autre fichier (ici le main)}
 *
 * \param arr tableau de réels qui vont etre trié, n un entier qui est la taille du tableau
 *
 */

void tri_insertion(float arr[], int n); /* Ne pas oublier le ";" */
void tri_bulle(float arr[], int n);
void tri_selection(float arr[],int n);
void valeur_aleatoire(float arr[],int n,int power);
#endif



