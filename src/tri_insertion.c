#include <stdio.h>
#include <stdlib.h>
#include "header.h"

/**
 * \fn tri_insertion(float arr[], int n)
 * \brief {méthode du trie à bulle pour un tableau de float}
 *
 * \param arr tableau de réels qui vont etre trié, n un entier qui est la taille du tableau
 * \return 0
 *
 */

void tri_insertion(float arr[], int n)
{
	int i, j;
	double k;

	for (i = 1; i < n; i++) {
		k = arr[i];

		for (j = i; j > 0 && arr[j - 1] > k; j--) {
			arr[j] = arr[j - 1];
		}
		arr[j] = k;
	}


}


