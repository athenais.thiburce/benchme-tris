#include <stdio.h>
#include <stdlib.h>
#include "header.h"

/**
 * \fn tri_bulle(float arr[], int n)
 * \brief {méthode du trie à bulle pour un tableau de float}
 *
 * \param arr tableau de réels qui vont etre trié, n un entier qui est la taille du tableau
 *
 */

void tri_bulle(float arr[], int n)  //fonctionne
{
	int i;
	{
		for(i=0;i<= n-1;i++)
		{
			for(int j=0;j<= n-i-1 - 1;j++){
				if(arr[j]>arr[j+1])
				{
					int tmp = arr[j+1];
					arr[j+1] = arr[j];
					arr[j] = tmp;
				}
			}

		}
	}
}
