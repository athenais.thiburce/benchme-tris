#include <stdio.h>
#include <stdlib.h>
#include "header.h"

void tri_selection(float arr[], int n)
{
	int test;
	int i, mini, j, x;
	for (i = 0; i < n; i++)
	{
		mini = i;
		for (j = i + 1; j < n; j++)
			if (arr[j] < arr[mini])
				mini = j;
		x = arr[i];
		arr[i] = arr[mini];
		arr[mini] = x;
	}
}
