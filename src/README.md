## Benchme-tris </br>
# Algo de Tries </br>

#Description : </br>
Ce projet consiste a avoir différent algorithme de tries : trie  </br>
à bulle, par insertion, selection et par pas.Et, pour chaque algorithme  </br>
on aura le temps d'exécution ainsi que les résultat stocké dans un fichier CSV </br>
ainsi qu'un documentation technique générée. </br></br>

#Usage </br>

**tri_insertion**  </br>
C'est une focntion qui est un algorithme qui insère un élément dans une liste d'éléments déjà triés. C'est la methode </br>
de trie la plus efficace pour des petites listes d'élements à trier ou que les éléments sont presque déjà trié. </br>

**tri à bulle** </br></br>
C'est une fonction qui est un algorithme qui consiste à faire remonter progressivement les plus grands éléments d'un tableau </br>
Sa complexité est de l'ordre de n² en moyenne (où n est la taille du tableau), ce qui le classe parmi les </br>
mauvais algorithmes de tri. </br>

**tri à selection** </br></br>
C'est une fonction qui est un algorithme de tri par comparaison. Il est particulièrement simple, mais inefficace sur de </br>
grandes entrées,car il s'exécute en temps quadratique en le nombre d'éléments à trier. </br>

**tri à pas** </br></br>
C'est un fonction qui est un algorithme qui utilise une structure de données temporaire dénommée "tas" comme mémoire de </br>
travail. C'est une variante de méthode de tri par sélection où l'on parcourt le tableau des éléments en sélectionnant et </br>
conservant les minimas successifs (plus petits éléments partiels) dans un arbre parfait partiellement ordonné. </br>
(Un arbre ici, est un type de graphe).</br></br>

# Résultat </br>

**Pour le trie à  insertion :** Triage réussi pour des tableau de float de manière rapide pour peu de nombre et le temps va augmenter </br>
pour réaliser le triage si il ya beaucoup de données à triées.C'est la plus rapide pour les petite donnée.</br>

**Pour le tri à bulle :** Triage réussi pour des tableau de float de manière rapide pour peu de nombre et le temps va augmenter </br>
pour réaliser le triage si il ya beaucoup de données à triées. </br>

**Pour le tri à selection :** Triage réussi pour des tableau de float de manière rapide pour peu de nombre et le temps va augmenter </br>
pour réaliser le triage si il ya beaucoup de données à triées. </br>

**Pour le tri par tas:** En attente </br>

#Evolution à venir

Finition de l'algorithme par tas 
Transfère fonctionnel de données dans un fichier CSV 
Génération de documentation technique




