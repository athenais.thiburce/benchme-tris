#include <stdio.h>
#include <stdlib.h>
#include "header.h"

void valeur_aleatoire (float arr[],int n,int power){
	srand((unsigned int)time(NULL));

	float a = pow(10,power);
	for (int i=0;i<n;i++){
		arr[i]= (float)rand()/(float) RAND_MAX * a;
	}
}
